#!/bin/bash
# Upendra Kumar Devisetty
# 02/24/16
# Script to process cuffcompare output file to generate lincRNA
# Usage: 
# sh evolinc-part-I.sh -c sample.data/cuffcompare_out_annot_no_annot.combined.gtf -g sample.data/Brassica_rapa_v1.2_genome.fa -r sample.data/Brassica_rapa_v1.2.gff -b sample.data/TE_RNA_transcripts.fa -t CAGE_file.txt -x 2> errors.txt > output.txt

usage() {
      echo ""
      echo "Usage : sh $0 -c cuffcompare -g genome -r gff -o output [-b TE_RNA] [-t CAGE_RNA] [-x Known_lincRNA]"
      echo ""

cat <<'EOF'
  -c </path/to/cuffcompare output file>

  -g </path/to/reference genome file>

  -r </path/to/reference annotation file>

  -b </path/to/Transposable Elements file>

  -o </path/to/output file>

  -t </path/to/CAGE RNA file>
  
  -x </path/to/Known lincRNA file>

  -h Show this usage information

EOF
    exit 0
}

while getopts ":b:c:g:hr:t:x:o:" opt; do
  case $opt in
    b)
      blastfile=$OPTARG
      ;;
    c)
      comparefile=$OPTARG
      ;;
    h)
      usage
      exit 1
      ;;    
    g)
     referencegenome=$OPTARG
      ;;
    r)
     referencegff=$OPTARG
      ;;  
    t)
     cagefile=$OPTARG
      ;;
    x)
     knownlinc=$OPTARG
      ;;
    o)
     output=$OPTARG
      ;;  
    \?)
      echo "Invalid option: -$OPTARG" >&2
      exit 1
      ;;
    :)
      echo "Option -$OPTARG requires an argument." >&2
      exit 1
      ;;
  esac
done

echo "BEGIN!"
echo `date`

START_TIME=$SECONDS

# Creat a directory to move all the output files
mkdir $output


# STEP 1:
START_TIME_1=$SECONDS
# Extracting classcode u transcripts, making fasta file, removing transcripts > 200 and selecting protein coding transcripts
grep -e '"u"' -e '"x"' -e '"s"' -e '"o"' -e '"e"' -e '"i"' $comparefile | gffread -w \
    transcripts_u.fa -g $referencegenome - && python /evolinc_docker/get_gene_length_filter.py transcripts_u.fa \
    transcripts_u_filter.fa && TransDecoder.LongOrfs -t transcripts_u_filter.fa


# Modifying the header
sed 's/ .*//' transcripts_u_filter.fa | sed -ne 's/>//p' > transcripts_u_filter.fa.genes

# Move the transcript files to this directory transcripts_u_filter.fa.transdecoder_dir
mv transcripts_u.fa transcripts_u_filter.fa transcripts_u_filter.fa.genes transcripts_u_filter.fa.transdecoder_dir/

# Change the directory 
cd transcripts_u_filter.fa.transdecoder_dir

# Genes in the protein coding genes
sed 's/|.*//' longest_orfs.cds | sed -ne 's/>//p' | uniq > longest_orfs.cds.genes

# Remove these protein coding genes from the filter file
grep -v -f longest_orfs.cds.genes transcripts_u_filter.fa.genes > transcripts_u_filter.not.genes && \
  sed 's/^/>/' transcripts_u_filter.not.genes > temp && mv temp transcripts_u_filter.not.genes # changed name here 

# Extract fasta file
python /evolinc_docker/extract_sequences.py transcripts_u_filter.not.genes transcripts_u_filter.fa transcripts_u_filter.not.genes.fa && \
  sed 's/ /./' transcripts_u_filter.not.genes.fa > temp && mv temp transcripts_u_filter.not.genes.fa

# Blast the fasta file to TE RNA db
for i in "$@" ; do
  if [[ $i == "$blastfile" ]] ; 
  then
     makeblastdb -in ../$blastfile -dbtype nucl -out ../$blastfile.blast.out &&
     blastn -query transcripts_u_filter.not.genes.fa -db ../$blastfile.blast.out -out transcripts_u_filter.not.genes.fa.blast.out -outfmt 6 # no blast hits her
  else
    touch transcripts_u_filter.not.genes.fa.blast.out
  fi
done

# Filter the output to select the best transcript based on e-value and bit-score
python /evolinc_docker/filter_sequences.py transcripts_u_filter.not.genes.fa.blast.out transcripts_u_filter.not.genes.fa.blast.out.filtered

# Modify the header in the fasta file to extract header only
grep ">" transcripts_u_filter.not.genes.fa | sed 's/>//' > transcripts_u_filter.not.genes_only

# Now remove the blast hits from the fasta file
python /evolinc_docker/fasta_remove.py transcripts_u_filter.not.genes.fa.blast.out.filtered transcripts_u_filter.not.genes_only lincRNA.genes

# Modify the fasta header to include ">"
sed 's/^/>/' lincRNA.genes > temp && mv temp lincRNA.genes

# Extract the sequences
python /evolinc_docker/extract_sequences-1.py lincRNA.genes transcripts_u_filter.not.genes.fa lincRNA.genes.fa

ELAPSED_TIME_1=$(($SECONDS - $START_TIME_1))
echo "Elapsed time for step 1 is" $ELAPSED_TIME_1 "seconds" > ../$output/output.txt

# STEP 2:
START_TIME_2=$SECONDS
# Convert lincRNA fasta to bed
# Index the genome first
bwa index ../$referencegenome ../$referencegenome

# Mapping the lincRNA to genome using bwa
bwa bwasw -t 2 ../$referencegenome lincRNA.genes.fa > lincRNA.genes.sam

# Convert sam to bam using samtools
samtools view -Shu lincRNA.genes.sam > lincRNA.genes.bam

# Convert bam to bed using bed tools
bamToBed -bed12 -i lincRNA.genes.bam > lincRNA.genes.bed

ELAPSED_TIME_2=$(($SECONDS - $START_TIME_2))
echo "Elapsed time for Step 2 is" $ELAPSED_TIME_2 "seconds" >> ../$output/output.txt

# STEP 3:
START_TIME_3=$SECONDS
# Identify transcripts that are overlapping in the same direction (OT)
intersectBed -a lincRNA.genes.bed -b ../$referencegff -u -s > OT.genes.all.bed

# Make a list from the above file
cut -f 4 OT.genes.all.bed | uniq | sed 's/^/>/' > OT.all.list.txt

# Now create a file of those that are overlapping more than 80%
intersectBed -a lincRNA.genes.bed -b ../$referencegff -u -s -f 0.8 > OT.genes.high.overlap.bed

# Make a list from the above file
cut -f 4 OT.genes.high.overlap.bed | uniq | sed 's/^/>/' > OT.to.remove.txt

# Remove the high overlapping transcripts from the general OTs
grep -vFf OT.to.remove.txt OT.all.list.txt > OT.to.keep.list.txt

# Move OT to a new file
python /evolinc_docker/extract_sequences-1.py OT.to.keep.list.txt lincRNA.genes.fa Overlapping.transcipts.fa

ELAPSED_TIME_3=$(($SECONDS - $START_TIME_3))
echo "Elapsed time for Step 3 is" $ELAPSED_TIME_3 "seconds" >> ../$output/output.txt

# STEP 4:
START_TIME_4=$SECONDS
# Identify transcripts that are overlapping in the opposite direction (NAT)
intersectBed -a lincRNA.genes.bed -b ../$referencegff -u -S > NAT.genes.all.bed

# Make a list from the above file
cut -f 4 NAT.genes.all.bed | uniq | sed 's/^/>/' > NAT.all.list.txt

# Now create a file of those that are overlapping more than 80%
intersectBed -a lincRNA.genes.bed -b ../$referencegff -u -S -f 0.8 > NAT.genes.high.overlap.bed

# Make a list from the above file
cut -f 4 NAT.genes.high.overlap.bed | uniq | sed 's/^/>/' > NAT.to.remove.txt

# Remove the high overlapping transcripts from the general OTs
grep -vFf NAT.to.remove.txt NAT.all.list.txt > NAT.to.keep.list.txt

# Move OT to a new file
python /evolinc_docker/extract_sequences-1.py NAT.to.keep.list.txt lincRNA.genes.fa Natural.antisense.transcipts.fa

ELAPSED_TIME_4=$(($SECONDS - $START_TIME_4))
echo "Elapsed time for Step 4 is" $ELAPSED_TIME_4 "seconds" >> ../$output/output.txt

# STEP 5:
START_TIME_5=$SECONDS
# Intersect bed to remove overlapping genes
intersectBed -a lincRNA.genes.bed -b ../$referencegff -v > lincRNA.genes.filtered.bed

# Make a list from the above file
cut -f 4 lincRNA.genes.filtered.bed | uniq | sed 's/^/>/' > lincRNA.genes.filtered.uniq.genes

# Move genes to a new file
python /evolinc_docker/extract_sequences-1.py lincRNA.genes.filtered.uniq.genes lincRNA.genes.fa All.lincRNAs.fa

# Sort the bed file
sortBed -i lincRNA.genes.filtered.bed > lincRNA.bed

ELAPSED_TIME_5=$(($SECONDS - $START_TIME_5))
echo "Elapsed time for Step 5 is" $ELAPSED_TIME_5 "seconds" >> ../$output/output.txt

# STEP 6: 
START_TIME_6=$SECONDS
# Promoter extraction
sed 's/>//' lincRNA.genes.filtered.uniq.genes | cut -d "." -f 1 > lincRNA.genes.filtered.uniq.genes.mod

# Extract the coordinates from the cuffcompare file
grep -f lincRNA.genes.filtered.uniq.genes.mod ../$comparefile > lincRNA.genes.filtered.genes.gtf

# Extracting promoter coordinates from the gtf file for the transcripts
python /evolinc_docker/prepare_promoter_gtf.py lincRNA.genes.filtered.genes.gtf lincRNA.genes.filtered.genes.promoters.gtf

# Extracting fasta from the promoter coordinates
gffread lincRNA.genes.filtered.genes.promoters.gtf -w lincRNA.upstream.sequence.fa -g ../$referencegenome

ELAPSED_TIME_6=$(($SECONDS - $START_TIME_6))
echo "Elapsed time for Step 6 is" $ELAPSED_TIME_6 "seconds" >> ../$output/output.txt

# STEP 7: 
START_TIME_7=$SECONDS
# Update the cufflinks gtf file
python /evolinc_docker/update_gtf.py All.lincRNAs.fa ../$comparefile lincRNA.updated.gtf

ELAPSED_TIME_7=$(($SECONDS - $START_TIME_7))
echo "Elapsed time for Step 7 is" $ELAPSED_TIME_7 "seconds" >> ../$output/output.txt

# STEP 8:
START_TIME_8=$SECONDS
# Demographics for all lincRNA
python /evolinc_docker/quast-3.0/quast.py All.lincRNAs.fa -o lincRNA.genes.filtered.genes_demographics
sed 's/contig/lincRNA/g' lincRNA.genes.filtered.genes_demographics/report.txt > lincRNA.demographics.txt

# Demographics for Overlapping lincRNA
python /evolinc_docker/quast-3.0/quast.py Overlapping.transcipts.fa -o Overlapping.transcipts_demographics
sed 's/contig/lincRNA/g' Overlapping.transcipts_demographics/report.txt > Overlapping.transcipts_demographics.txt

# Demographics for natural antisense lincRNA
python /evolinc_docker/quast-3.0/quast.py Natural.antisense.transcipts.fa -o Natural.antisense.transcipts.fa_demographics
sed 's/contig/lincRNA/g' Natural.antisense.transcipts.fa_demographics/report.txt > Natural.antisense.transcipts_demographics.txt

ELAPSED_TIME_8=$(($SECONDS - $START_TIME_8))
echo "Elapsed time for Step 8 is" $ELAPSED_TIME_8 "seconds" >> ../$output/output.txt

# STEP 9:
START_TIME_9=$SECONDS
# Copy the files to the outputfiles
cp lincRNA.bed All.lincRNAs.fa lincRNA.upstream.sequence.fa lincRNA.demographics.txt lincRNA.updated.gtf Overlapping.transcipts.fa Overlapping.transcipts_demographics.txt Natural.antisense.transcipts.fa Natural.antisense.transcipts_demographics.txt ../$output

ELAPSED_TIME_9=$(($SECONDS - $START_TIME_9))
echo "Elapsed time for Step 9 is" $ELAPSED_TIME_9 "seconds" >> ../$output/output.txt

# Optional STEP - 1:
START_TIME_O1=$SECONDS
# CAGE data
for i in "$@" ; do
  if [[ $i == "$cagefile" ]] ; then
     python /evolinc_docker/gff2bed.py ../$cagefile AnnotatedPEATPeaks.bed &&
     sortBed -i AnnotatedPEATPeaks.bed > AnnotatedPEATPeaks.sorted.bed &&
     closestBed -a lincRNA.bed -b AnnotatedPEATPeaks.sorted.bed -s -D a > closest_output.txt &&       
     python /evolinc_docker/closet_bed_compare.py closest_output.txt All.lincRNAs.fa lincRNAs.with.CAGE.support.annotated.fa &&
     cp lincRNAs.with.CAGE.support.annotated.fa ../$output
 fi
done

ELAPSED_TIME_O1=$(($SECONDS - $START_TIME_O1))
echo "Elapsed time for Optional Step 1 is" $ELAPSED_TIME_O1 "seconds" >> ../$output/output.txt

# Optional STEP - 2:
START_TIME_O2=$SECONDS
# Known lincRNA
for i in "$@" ; do
  if [[ $i == "$knownlinc" ]] ; then
     python /evolinc_docker/gff2bed.py ../$knownlinc Atha_known_lncRNAs.bed &&
     sortBed -i Atha_known_lncRNAs.bed > Atha_known_lncRNAs.sorted.bed &&
     intersectBed -a lincRNA.bed -b Atha_known_lncRNAs.sorted.bed > intersect_output.txt &&
     python /evolinc_docker/interesect_bed_compare.py intersect_output.txt All.lincRNAs.fa lincRNAs.overlapping.known.lincs.fa &&
     cp lincRNAs.overlapping.known.lincs.fa ../$output
  fi
done

ELAPSED_TIME_O2=$(($SECONDS - $START_TIME_O2))
echo "Elapsed time for Optional Step 2 is" $ELAPSED_TIME_O2 "seconds" >> ../$output/output.txt

# Pie chart if both CAGE and Knownlinc are given
if [ ! -z $cagefile ] && [ ! -z $knownlinc ] ; then
   python /evolinc_docker/lincRNA_fig.py All.lincRNAs.fa lincRNAs.with.CAGE.support.annotated.fa lincRNAs.overlapping.known.lincs.fa &&
   cp lincRNA_piechart.png ../$output
fi

# remove all the other files
rm -r ../transcripts_u_filter.fa.transdecoder_dir
rm ../*.fa*.*

echo "All necessary files written to" $output
echo "Finished Evolinc-part-I!"
echo `date`

ELAPSED_TIME=$(($SECONDS - $START_TIME))
echo "Total elapsed time is" $ELAPSED_TIME "seconds" >> ../$output/output.txt
